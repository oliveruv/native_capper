#ifndef LINUXSCREENCAPPER_H_PNABFI4H
#define LINUXSCREENCAPPER_H_PNABFI4H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

// TODO Create an interface Screencapper that all native cappers fulfill.

class LinuxScreencapper {
    public:
        typedef struct FrameInfo {
            unsigned int mouse_x;
            unsigned int mouse_y;
            unsigned long frame_duration_ms;
            unsigned long frame_timestamp_ms;

            void copy(const FrameInfo &from) {
                this->mouse_x = from.mouse_x;
                this->mouse_y = from.mouse_y;
                this->frame_duration_ms = from.frame_duration_ms;
                this->frame_timestamp_ms = from.frame_timestamp_ms;
            }
        } FrameInfo;

        void bootstrap(
                int width,
                int height,
                int taget_bitrate,
                int screen,
                // This byte_array has to be allocated by the caller,
                // and should be 32 * sizeof(unsigned char) in size. (32 bytes.)
                unsigned char *byte_array,
                FrameInfo *frame_info_out);
        void fetch(unsigned char *byte_array_out, int frame_size, FrameInfo *frame_info_out);
        int capture();
        void cleanup();

    private:
        Display *dpy;
        int screen;
        Window target_win;
        Window *root_windows;
        int w;
        int h;
        int arr_argb_size;
        int arr_rgb_size;
        int number_of_screens;
        unsigned char* outframe;
        bool outframe_dirty;
        FrameInfo frame_info;

        timespec last_timing;
        unsigned long total_time;

        void set_frame_info(unsigned long frame_duration, unsigned long total_duration);
        unsigned long get_frame_duration();

};

#endif /* end of include guard: LINUXSCREENCAPPER_H_PNABFI4H */
