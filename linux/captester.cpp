#include <iostream>
#include <fstream>
#include <cstdio>

#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/X.h>
#include <time.h>

// For chmod()
#include <sys/types.h>
#include <sys/stat.h>

#include "libsc.h"

using namespace std;

static double timespec_diff(timespec start, timespec end) {
    unsigned long diff;
    timespec temp;
    if (end.tv_nsec - start.tv_nsec < 0) {
        // We need to borrow 1 sec so the nsec subtraction's
        // difference doesn't end up in the negatives.
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    diff = (double) temp.tv_sec;
    diff += (double) (temp.tv_nsec / 1000000000);
    return diff;
}

int main(int argc, char *argv[])
{
    Display *disp;
    Window root;
    LinuxScreencapper::FrameInfo frame_info;
    int scr;
    int screenshots = 210;
    int target_bitrate = 10; // kbps TODO figure out why higher values produce worse output and lower fps!
    int target_capture_bitrate  = target_bitrate * 8 * 1024; // bitrate in bps (*8*1024 = KiBps)
                                                             // independent from screencapping. Specified in KiBps!
    // Constant number of pixels to capture for profiling purposes.
    // Make sure this is <= your current resolution.
    int width = 1600;
    int height = 900;

    disp = XOpenDisplay(NULL);
    if (!disp) {
        fprintf(stderr, "ERROR: Could not open display!\n");
        exit(1);
    }
    scr = XDefaultScreen(disp);
    root = XRootWindow(disp, scr);

    XWindowAttributes attr;
    XGetWindowAttributes(disp, root, &attr);

    XCloseDisplay(disp);

    // Uncomment to capture whole screen.
    /* width = attr.width; */
    /* height = attr.height; */

    LinuxScreencapper capper = LinuxScreencapper();

    timespec start_time;
    clock_gettime(CLOCK_REALTIME, &start_time);

    unsigned char* header_buffer = (unsigned char*) malloc(32 * sizeof (unsigned char));
    capper.bootstrap(
            width,
            height,
            target_capture_bitrate,
            scr,
            header_buffer,
            &frame_info);

    ofstream ss;
    ss.open("00000.ivf", ios::out | ios::binary);
    ss.write(reinterpret_cast<const char*>(header_buffer), 32);
    ss.close();
    free(header_buffer);

    for (int i = 1; i < screenshots; ++i)
    {
        int frame_size = capper.capture();
        unsigned char* frame_buffer = (unsigned char*) malloc(frame_size * sizeof (unsigned char));
        capper.fetch(frame_buffer, frame_size, &frame_info);
        char filename[50];
        int res;
        res = sprintf(filename, "%05d.ivf", i);

        ofstream ff;
        ff.open(filename, ios::out | ios::binary);
        ff.write(reinterpret_cast<const char*>(frame_buffer), frame_size);
        ff.close();
        free(frame_buffer);
        fprintf(stdout, "x: %d, y: %d, length: %lu, time: %lu\n",
                frame_info.mouse_x,
                frame_info.mouse_y,
                frame_info.frame_duration_ms,
                frame_info.frame_timestamp_ms
                );
    }

    timespec end_time;
    clock_gettime(CLOCK_REALTIME, &end_time);
    double elapsed_secs = timespec_diff(start_time, end_time);
    double fps = ((double) screenshots) / elapsed_secs;

    // Print info text
    char info_text[200];
    int res;
    res = sprintf(info_text, "Recorded for %f seconds, a total of %d frames, for %f fps.", elapsed_secs, screenshots, fps);
    cout << endl << "/////////////////" << endl << info_text << endl;

    // Build ivf merging and webm wrapping script
    const char *script_path = "makemov.sh";
    FILE *script = fopen(script_path, "w");
    const char *cat_command = "cat 0*.ivf > movie.ivf";
    char ffmpeg_command[200] = "ggmpeg -i movie.ivf -vcodec copy -f webm movie.webm";

    if (script == NULL) {
        printf("\nError creating helperscript.\nRun manually:\n");
        cout << cat_command << endl;
        cout << ffmpeg_command << endl;
    } else {
        printf("To rebuild file run %s\n", script_path);
        fprintf(script, "#! /bin/sh\n");
        fprintf(script, "%s\n", cat_command);
        fprintf(script, "%s\n", ffmpeg_command);
        fclose(script);
        chmod(script_path, S_IRWXU);
    }

    capper.cleanup();
    return 0;
}
