/*
 *  Copyright (c) 2010 The WebM project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */


/*
 * This is an example of a simple encoder loop. It takes an input file in
 * YV12 format, passes it through the encoder, and writes the compressed
 * frames to disk in IVF format. Other decoder examples build upon this
 * one.
 * 
 * The details of the IVF format have been elided from this example for
 * simplicity of presentation, as IVF files will not generally be used by
 * your application. In general, an IVF file consists of a file header,
 * followed by a variable number of frames. Each frame consists of a frame
 * header followed by a variable length payload. The length of the payload
 * is specified in the first four bytes of the frame header. The payload is
 * the raw compressed data.
 */
#include "webm_enc.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#define VPX_CODEC_DISABLE_COMPAT 1
#include "vpx/vpx_encoder.h"
#include "vpx/vp8cx.h"


#define interface (vpx_codec_vp8_cx())

#define IVF_FILE_HDR_SZ  (32)
#define IVF_FRAME_HDR_SZ (12)

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "libyuv.h"

static void mem_put_le16(char *mem, unsigned int val) {
    mem[0] = val;
    mem[1] = val>>8;
}

static void mem_put_le32(char *mem, unsigned int val) {
    mem[0] = val;
    mem[1] = val>>8;
    mem[2] = val>>16;
    mem[3] = val>>24;
}

static void die(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    vprintf(fmt, ap);
    if(fmt[strlen(fmt)-1] != '\n')
        printf("\n");
    exit(EXIT_FAILURE);
}

static void die_codec(vpx_codec_ctx_t *ctx, const char *s) {                 
    const char *detail = vpx_codec_error_detail(ctx);                        

    printf("%s: %s\n", s, vpx_codec_error(ctx));                             
    if(detail)                                                                
        printf("    %s\n",detail);                                           
    exit(EXIT_FAILURE);                                                       
}               

void do_color_conversion(const unsigned char* src_rgb, int src_stride_rgb24,
        unsigned char* dst_y, int dst_stride_y,
        unsigned char* dst_u, int dst_stride_u,
        unsigned char* dst_v, int dst_stride_v,
        int width, int height)
{
    libyuv::ARGBToI420(
            (const uint8*) src_rgb, src_stride_rgb24,
            (uint8*) dst_y, dst_stride_y,
            (uint8*) dst_u, dst_stride_u,
            (uint8*) dst_v, dst_stride_v,
            width, height);
}

static void write_ivf_file_header(char* header,
        const vpx_codec_enc_cfg_t *cfg,
        int frame_cnt) {



    if(cfg->g_pass != VPX_RC_ONE_PASS && cfg->g_pass != VPX_RC_LAST_PASS)
        return;
    header[0] = 'D';
    header[1] = 'K';
    header[2] = 'I';
    header[3] = 'F';
    mem_put_le16(header+4,  0);                   /* version */
    mem_put_le16(header+6,  32);                  /* headersize */
    mem_put_le32(header+8,  0x30385056);          /* headersize, was earlier #def as 'fourcc' */
    mem_put_le16(header+12, cfg->g_w);            /* width */
    mem_put_le16(header+14, cfg->g_h);            /* height */
    /* mem_put_le32(header+16, 1000);//cfg->g_timebase.den); /1* rate *1/ */
    mem_put_le32(header+16, cfg->g_timebase.den);
    mem_put_le32(header+20, cfg->g_timebase.num); /* scale */
    mem_put_le32(header+24, frame_cnt);           /* length, seems unsused? */
    mem_put_le32(header+28, 0);                   /* unused */
}

vpx_codec_ctx_t      codec;
vpx_codec_enc_cfg_t  cfg;
int                  frame_cnt = 0;
vpx_image_t          raw;
vpx_codec_err_t      res;
int                  frame_avail;
int                  got_data;
int                  flags = 0;


void webm_setup(int width, int height, int target_bitrate, char** outframe)
{
    vpx_codec_enc_config_default(interface, &cfg, 0);

    cfg.g_w = width;
    cfg.g_h = height;
    cfg.rc_target_bitrate = target_bitrate;
    // Set timebase to be in milliseconds.
    cfg.g_timebase.num = 1;
    cfg.g_timebase.den = 1000;

    if(vpx_codec_enc_init(&codec, interface, &cfg, 0)) {
        /* printf("Fatal: Could not init codec\n"); */
        die_codec(&codec, "Fatal: Could not init codec\n");
    }

    vpx_img_alloc(&raw, VPX_IMG_FMT_YV12, width, height, 1);

    *outframe = (char*) malloc(32); 
    write_ivf_file_header(*outframe, &cfg, 0);
}

static void write_ivf_frame_header(char* header,
        const vpx_codec_cx_pkt_t *pkt)
{
    //char             header[12];
    vpx_codec_pts_t  pts;

    if(pkt->kind != VPX_CODEC_CX_FRAME_PKT)
        return;

    pts = pkt->data.frame.pts;
    mem_put_le32(header, pkt->data.frame.sz);
    mem_put_le32(header+4, pts&0xFFFFFFFF);
    mem_put_le32(header+8, pts >> 32);
}

int frame_pts = 0;
long last_ts = 0;

// WARNING: Will malloc outframe but not free it!
// frame_duration in timebase units, which is specified in webm_setup.
int webm_encode(unsigned char* img_data, int width, int height, unsigned long frame_duration, long timestamp, char** outframe)
{
    // ASSUMES that we get an ARGB format image. I'm not certain
    // that this assumption holds for all nix clients.
    do_color_conversion(img_data, width*4,
            raw.planes[0],raw.stride[0],
            raw.planes[1],raw.stride[1],
            raw.planes[2],raw.stride[2],		   
            width, height);

    vpx_codec_iter_t iter = NULL;
    const vpx_codec_cx_pkt_t *pkt;

    if(last_ts != 0)
        frame_pts = timestamp - last_ts + frame_pts;
    last_ts = timestamp;

    if(vpx_codec_encode(
                &codec,                      // context
                &raw,                        // image
                (vpx_codec_pts_t) timestamp, // frame presentation timestamp
                frame_duration,              // frame duration (timebase units)
                0,                           // Flags
                40))                          // Deadline for frame encoding in ms, 0 = no deadline
            die_codec(&codec, "Failed to encode frame\n");                  

    int size = 0;
    while( (pkt = vpx_codec_get_cx_data(&codec, &iter)) ) {

        switch(pkt->kind) {  // This happens once per call to webm_encode.
            case VPX_CODEC_CX_FRAME_PKT:
                size = pkt->data.frame.sz;
                *outframe = (char*) malloc(12+pkt->data.frame.sz);
                write_ivf_frame_header(*outframe, pkt);
                *outframe += 12;
                memcpy(*outframe, pkt->data.frame.buf, pkt->data.frame.sz);

                *outframe -=12;
                // The pointer move above probably only matters if this block
                // is executed more than once per webm_encode call. But then
                // things will probably break in other ways anyway (or we'll
                // leak memory for sure.
                break;                                         
            default:
                break;
        }
    }
    frame_cnt++;
    //if(vpx_codec_destroy(&codec))
    //  die_codec(&codec, "Failed to destroy codec");
    return size+12;
}
