# We no longer use libcairo to build our native code, but this file is
# about halfway through an attempt at statically linking a library into
# our screencapping library. This is something we will have to do with
# libvpx soon enough, so hopefully this will be somewhat useful at that
# point.

# This is a modification of the original makefile where we're trying to
# prepare for statically linking libcairo and pixman (which libcairo
# depends on) with libsc so that linux clients won't need to have the
# correct version of libcairo installed on their systems already.
#
# To get this to work we need to:
#  * Compile libcairo ourselves, since the static version does not have
#    -fPIC turned on.
#  * Properly link shizzle.

# REQUIREMENTS
# - libcairo2-dev -> use at least version 1.12.14 ! Check which version is used with ldd.
# - libx11-dev

SHELL         = /bin/sh
CC            = gcc
LINKER        = gcc

FLAGS         = 
CFLAGS        = -fPIC -c
LDFLAGS       = -shared
DEBUGFLAGS    = -ggdb
LIBS          = $(shell pkg-config --libs --cflags x11) $(shell pkg-config --cflags cairo cairo-xlib) -lstdc++
STATICCAIRO   = /usr/local/lib/libcairo.a /usr/local/lib/libpixman-1.a

# For the shared library to work the libsc.so file's parent directory must be in your LD_LIBRARY_PATH.
# Check whether your system can find this library by executing `ldd shared_captester`.
SCLIB          = -L. -lsc

all: libsc.so captester shared_captester

captester: captester.cpp libsc.o
	$(CC) $(DEBUGFLAGS) -o $@ $^ $(LIBS)

shared_captester: captester.cpp libsc.so
	$(CC) $(FLAGS) $(DEBUGFLAGS) -o $@ $< $(LIBS) $(SCLIB)

libsc.so: libsc.o
	$(LINKER) $(FLAGS) $(LDFLAGS) $(DEBUGFLAGS) -o $@ $^ $(STATICCAIRO) $(LIBS)

libsc.o: libsc.cpp
	$(CC) $(FLAGS) $(CFLAGS) $(DEBUGFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f *.o *.so *.png captester shared_captester

